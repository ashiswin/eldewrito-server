#include <string>
#include <vector>

class HttpRequest
{
private:
	std::wstring _userAgent;
public:
	int lastError;

	HttpRequest(const std::wstring &userAgent);
	bool SendRequest(const std::string &uri, const std::wstring &method, const std::wstring &username, const std::wstring &password, const std::wstring &headers, void *body, long bodySize);
	std::wstring responseHeader;
	std::vector<char> responseBody;
};