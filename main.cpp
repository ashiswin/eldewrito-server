#include <stdlib.h>
#include <iostream>
#include "Announcer.hpp"
#include "Server.hpp"
#include "GameServer.hpp"

void *ListenThread(void* param) {
	Server *s = new Server(11775);
	int client = s->Listen();
	s->ProcessMessage(client);
}

void *GameServerListenThread(void* param) {
	GameServer *s = new GameServer(11774);
	while(1) {
		int client = s->Listen();
		s->ProcessMessage(client);
	}
}

int main() {
	// Initialise info server
	pthread_t thread;
	pthread_create(&thread, NULL, ListenThread, NULL);

	// TODO: Game server does not work
	/*pthread_t gameserverThread;
	pthread_create(&gameserverThread, NULL, GameServerListenThread, NULL);*/

	Announcer announcer;

	announcer.Announce();

	while(1) {}
}