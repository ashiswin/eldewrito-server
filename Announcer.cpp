#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "HttpRequest.hpp"
#include "Announcer.hpp"
#include "String.hpp"
#include "VersionInfo.hpp"

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <pthread.h>

void *CommandServerAnnounce_Thread(void* param)
{
	std::stringstream ss;
	std::string resp;
	std::vector<std::string> announceEndpoints;
	Announcer* announcer = (Announcer*) param;

	announcer->GetEndpoints(announceEndpoints, "announce");

	for (auto server : announceEndpoints)
	{
		HttpRequest req(L"ElDewrito/" + Utils::String::WidenString(Utils::Version::GetVersionString()));

		try
		{
			if (!req.SendRequest(server + "?port=11724", L"GET", L"", L"", L"", NULL, 0))
			{
				ss << "Unable to connect to master server " << server << " (error: " << req.lastError << ")" << std::endl << std::endl;
				continue;
			}
		}
		catch(...) // TODO: find out what exception is being caused
		{
			ss << "Exception during master server announce request to " << server << std::endl << std::endl;
			continue;
		}

		// make sure the server replied with 200 OK
		std::wstring expected = L"HTTP/1.1 200 OK";
		while(req.responseHeader.length() == 0) {}
		std::cout << req.responseHeader.length() << " " << expected.length() << std::endl;
		if (req.responseHeader.length() < expected.length())
		{
			ss << "Invalid master server announce response 1 from " << server << std::endl << std::endl;
			continue;
		}

		auto respHdr = req.responseHeader.substr(0, expected.length());
		if (respHdr.compare(expected))
		{
			ss << "Invalid master server announce response 2 from " << server << std::endl << std::endl;
			continue;
		}

		// parse the json response
		resp = std::string(req.responseBody.begin(), req.responseBody.end());
		rapidjson::Document json;
		if (json.Parse<0>(resp.c_str()).HasParseError() || !json.IsObject())
		{
			ss << "Invalid master server JSON response from " << server << std::endl << std::endl;
			continue;
		}

		if (!json.HasMember("result"))
		{
			ss << "Master server JSON response from " << server << " is missing data." << std::endl << std::endl;
			continue;
		}

		auto& result = json["result"];
		if (result["code"].GetInt() != 0)
		{
			ss << "Master server " << server << " returned error code " << result["code"].GetInt() << " (" << result["msg"].GetString() << ")" << std::endl << std::endl;
			continue;
		}
	}

	std::string errors = ss.str();
	if (errors.length() > 0)
		std::cout << "Announce: " << ss.str() << std::endl;
	else
		std::cout << "Announce: Server announced!\nServer Response: " << resp << std::endl;
	pthread_exit(NULL);
}

// retrieves master server endpoints from dewrito.json
void Announcer::GetEndpoints(std::vector<std::string>& destVect, std::string endpointType)
{
	std::ifstream in("dewrito.json", std::ios::in | std::ios::binary);
	if (in && in.is_open())
	{
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize((unsigned int)in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();

		rapidjson::Document json;
		if (!json.Parse<0>(contents.c_str()).HasParseError() && json.IsObject())
		{
			if (json.HasMember("masterServers"))
			{
				auto& mastersArray = json["masterServers"];
				for (auto it = mastersArray.Begin(); it != mastersArray.End(); it++)
				{
					destVect.push_back((*it)[endpointType.c_str()].GetString());
				}
			}
		}
	}
}

void Announcer::Announce() {
	pthread_t thread;
	pthread_create(&thread, NULL, CommandServerAnnounce_Thread, (void*)this);
}