#ifndef SERVER_HPP
#define SERVER_HPP

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sstream>

class Server {
	private:
		int sockfd, newsockfd, port;
		struct sockaddr_in serv_addr, cli_addr;
	public:
		Server(int port);
		int Listen();
		void ProcessMessage(int clientSocket);
		void SendServerInfo(int clientSocket);
};

#endif