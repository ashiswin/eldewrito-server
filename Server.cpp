#include <iostream>
#include "Server.hpp"
#include "VersionInfo.hpp"
#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "String.hpp"
#include "Blam/BlamTypes.hpp"

Server::Server(int port) {
	this->port = port;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	bzero((char*)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		std::cout << "Error binding socket to port " << port << std::endl;
		return;
	}
	listen(sockfd, 5);
	std::cout << "Listening on port " << port << std::endl;
}

int Server::Listen() {
	socklen_t clilen = sizeof(cli_addr);
	int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

	if(newsockfd < 0) {
		std::cout << "Error accepting client" << std::endl;
		return -1;
	}

	return newsockfd;
}

void Server::ProcessMessage(int clientSocket) {
	char buffer[1024];
	bzero(buffer, 1024);
	int n = read(clientSocket, buffer, 1023);

	if(n < 0) {
		std::cout << "Error reading from socket in Server" << std::endl;
		return;
	}

	std::string message = std::string(buffer);

	//std::cout << "Received:\n" << message << std::endl;

	std::string method = message.substr(0, message.find(' '));
	std::string path;
	std::stringstream ss;
	for(int i = message.find(' ') + 1; i < message.length(); i++) {
		if(message[i] == ' ') break;
		ss << message[i];
	}
	path = ss.str();

	//std::cout << "Method: " << method << "\nPath: " << path << std::endl;

	if(method.compare("GET") == 0 && path.compare("/") == 0) {
		SendServerInfo(clientSocket);
	}
}

void Server::SendServerInfo(int clientSocket) {
	//std::string mapName((char*)Pointer(0x22AB018)(0x1A4));
	std::string mapName("icebox");
	//std::wstring mapVariantName((wchar_t*)Pointer(0x1863ACA));
	std::wstring mapVariantName(Utils::String::WidenString("icebox2"));
	//std::wstring variantName((wchar_t*)Pointer(0x23DAF4C));
	std::wstring variantName(Utils::String::WidenString("icebox3"));
	std::string xnkid;
	std::string xnaddr;
	//std::string gameVersion((char*)Pointer(0x199C0F0));
	std::string gameVersion(Utils::Version::GetVersionString());
	std::string status = "InGame";
	//Utils::String::BytesToHexString((char*)Pointer(0x2247b80), 0x10, xnkid);
	Utils::String::BytesToHexString((void*)"aaaaaaaa\0", 0x10, xnkid);
	//Utils::String::BytesToHexString((char*)Pointer(0x2247b90), 0x10, xnaddr);
	Utils::String::BytesToHexString((void*)"bbbbbbbb\0", 0x10, xnaddr);

	//Pointer &gameModePtr = ElDorito::GetMainTls(GameGlobals::GameInfo::TLSOffset)[0](GameGlobals::GameInfo::GameMode);
	//uint32_t gameMode = gameModePtr.Read<uint32_t>();
	uint32_t gameMode = 6;
	//int32_t variantType = Pointer(0x023DAF18).Read<int32_t>();
	int32_t variantType = 7;
	
	/*if (gameMode == 3)
	{
		if (mapName == "mainmenu")
		{
			status = "InLobby";
			// on mainmenu so we'll have to read other data
			mapName = std::string((char*)Pointer(0x19A5E49));
			variantName = std::wstring((wchar_t*)Pointer(0x179254));
			variantType = Pointer(0x179250).Read<uint32_t>();
		}
		else // TODO: find how to get the variant name/type while it's on the loading screen
			status = "Loading";
	}*/

	rapidjson::StringBuffer s;
	rapidjson::Writer<rapidjson::StringBuffer> writer(s);
	writer.StartObject();
	writer.Key("name");
	//writer.String(Modules::ModuleServer::Instance().VarServerName->ValueString.c_str());
	writer.String("Ashiserver");
	writer.Key("port");
	//writer.Int(Pointer(0x1860454).Read<uint32_t>());
	writer.String("11725");
	writer.Key("hostPlayer");
	//writer.String(Modules::ModulePlayer::Instance().VarPlayerName->ValueString.c_str());
	writer.String("ashiswin");
	writer.Key("sprintEnabled");
	//writer.String(Modules::ModuleServer::Instance().VarServerSprintEnabled->ValueString.c_str());
	writer.String("true");
	writer.Key("sprintUnlimitedEnabled");
	//writer.String(Modules::ModuleServer::Instance().VarServerSprintUnlimited->ValueString.c_str());
	writer.String("true");
	writer.Key("dualWielding");
	//writer.String(Modules::ModuleServer::Instance().VarServerDualWieldEnabled->ValueString.c_str());
	writer.String("true");
	writer.Key("assassinationEnabled");
	//writer.String(Modules::ModuleServer::Instance().VarServerAssassinationEnabled->ValueString.c_str());
	writer.String("true");
	writer.Key("VoIP");
	//writer.Bool(IsVoIPServerRunning() ? TRUE : FALSE);
	writer.Bool(false);

	//auto session = Blam::Network::GetActiveSession();
	//if (session && session->IsEstablished()){
	//	writer.Key("teams");
	//	writer.Bool(session->HasTeams());
	//}
	writer.Key("map");
	writer.String(Utils::String::ThinString(mapVariantName).c_str());
	writer.Key("mapFile");
	writer.String(mapName.c_str());
	writer.Key("variant");
	writer.String(Utils::String::ThinString(variantName).c_str());
	if (variantType >= 0 && variantType < Blam::GameTypeCount)
	{
		writer.Key("variantType");
		writer.String(Blam::GameTypeNames[variantType].c_str());
	}
	writer.Key("status");
	writer.String(status.c_str());
	writer.Key("numPlayers");
	//writer.Int(GetNumPlayers());
	writer.Int(15);

	// TODO: find how to get actual max players from the game, since our variable might be wrong
	writer.Key("maxPlayers");
	//writer.Int(Modules::ModuleServer::Instance().VarServerMaxPlayers->ValueInt);
	writer.Int(16);

	bool authenticated = false;
	/*if (!Modules::ModuleServer::Instance().VarServerPassword->ValueString.empty())
	{
		std::string authString = "dorito:" + Modules::ModuleServer::Instance().VarServerPassword->ValueString;
		authString = "Authorization: Basic " + Utils::String::Base64Encode((const unsigned char*)authString.c_str(), authString.length()) + "\r\n";
		authenticated = std::string(inDataBuffer).find(authString) != std::string::npos;
	}*/

	/*if(authenticated)
	{
		writer.Key("xnkid");
		writer.String(xnkid.c_str());
		writer.Key("xnaddr");
		writer.String(xnaddr.c_str());
		writer.Key("players");

		writer.StartArray();
		uint32_t playerScoresBase = 0x23F1724;
		//uint32_t playerInfoBase = 0x2162DD0;
		uint32_t playerInfoBase = 0x2162E08;
		uint32_t menuPlayerInfoBase = 0x1863B58;
		uint32_t playerStatusBase = 0x2161808;
		for (int i = 0; i < 16; i++)
		{
			uint16_t score = Pointer(playerScoresBase + (1080 * i)).Read<uint16_t>();
			uint16_t kills = Pointer(playerScoresBase + (1080 * i) + 4).Read<uint16_t>();
			uint16_t assists = Pointer(playerScoresBase + (1080 * i) + 6).Read<uint16_t>();
			uint16_t deaths = Pointer(playerScoresBase + (1080 * i) + 8).Read<uint16_t>();

			wchar_t* name = Pointer(playerInfoBase + (5696 * i));
			std::string nameStr = Utils::String::ThinString(name);

			wchar_t* menuName = Pointer(menuPlayerInfoBase + (0x1628 * i));
			std::string menuNameStr = Utils::String::ThinString(menuName);

			uint32_t ipAddr = Pointer(playerInfoBase + (5696 * i) - 88).Read<uint32_t>();
			uint16_t team = Pointer(playerInfoBase + (5696 * i) + 32).Read<uint16_t>();
			uint16_t num7 = Pointer(playerInfoBase + (5696 * i) + 36).Read<uint16_t>();

			uint8_t alive = Pointer(playerStatusBase + (176 * i)).Read<uint8_t>();

			uint64_t uid = Pointer(playerInfoBase + (5696 * i) - 8).Read<uint64_t>();
			std::string uidStr;
			Utils::String::BytesToHexString(&uid, sizeof(uint64_t), uidStr);

			if (menuNameStr.empty() && nameStr.empty() && ipAddr == 0)
				continue;

			writer.StartObject();
			writer.Key("name");
			writer.String(menuNameStr.c_str());
			writer.Key("score");
			writer.Int(score);
			writer.Key("kills");
			writer.Int(kills);
			writer.Key("assists");
			writer.Int(assists);
			writer.Key("deaths");
			writer.Int(deaths);
			writer.Key("team");
			writer.Int(team);
			writer.Key("isAlive");
			writer.Bool(alive == 1);
			writer.Key("uid");
			writer.String(uidStr.c_str());
			writer.EndObject();
		}
		writer.EndArray();
	}
	else
	{*/
		writer.Key("passworded");
		writer.Bool(true);
	//}

	writer.Key("gameVersion");
	writer.String(gameVersion.c_str());
	writer.Key("eldewritoVersion");
	writer.String(Utils::Version::GetVersionString().c_str());
	writer.EndObject();

	std::string replyData = s.GetString();
	std::string reply = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\nAccess-Control-Allow-Origin: *\r\nServer: ElDewrito/" + Utils::Version::GetVersionString() + "\r\nContent-Length: " + std::to_string(replyData.length()) + "\r\nConnection: close\r\n\r\n" + replyData;
	int n = write(clientSocket, reply.c_str(), reply.length());
	if(n < 0 ) {
		std::cout << "Error writing to socket" << std::endl;
	}
	else {
		std::cout << "Sent response of " << n << " bytes" << std::endl;
	}
}