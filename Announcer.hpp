#ifndef ANNOUNCER_HPP
#define ANNOUNCER_HPP

#include <string>
#include <vector>

class Announcer {
	public:
		void GetEndpoints(std::vector<std::string>& destVect, std::string endpointType);
		void Announce();
};

#endif