#include <iostream>
#include "GameServer.hpp"
#include "VersionInfo.hpp"
#include "rapidjson/rapidjson.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "String.hpp"
#include "Blam/BlamTypes.hpp"

GameServer::GameServer(int port) {
	this->port = port;
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	bzero((char*)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(port);
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		std::cout << "Error binding socket to port " << port << std::endl;
		return;
	}
	listen(sockfd, 5);
	std::cout << "Listening on port " << port << std::endl;
}

int GameServer::Listen() {
	socklen_t clilen = sizeof(cli_addr);
	int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);

	if(newsockfd < 0) {
		std::cout << "Error accepting client" << std::endl;
		return -1;
	}

	return newsockfd;
}

void GameServer::ProcessMessage(int clientSocket) {
	char buffer[1024];
	bzero(buffer, 1024);
	int n = read(clientSocket, buffer, 1023);

	if(n < 0) {
		std::cout << "Error reading from socket" << std::endl;
		return;
	}

	std::string message = std::string(buffer);

	std::cout << "Received:\n" << message << std::endl;

	std::string method = message.substr(0, message.find(' '));
	std::string path;
	std::stringstream ss;
	for(int i = message.find(' ') + 1; i < message.length(); i++) {
		if(message[i] == ' ') break;
		ss << message[i];
	}
	path = ss.str();

	std::cout << "Method: " << method << "\nPath: " << path << std::endl;

	/*if(method.compare("GET") == 0 && path.compare("/") == 0) {
		SendServerInfo(clientSocket);
	}*/
}

void GameServer::SendServerInfo(int clientSocket) {
	
}